#!/usr/bin/env python

import poplib
import email
import time
import re
import os
from subprocess import call

class GmailHelper:

    def __init__(self, username, password, download_folder = '/Users/gcopley/Emeals'):
        self.username = username
        self.password = password
        self.download_folder = download_folder
        self.download_file_name = ""
        self.gmail = poplib.POP3_SSL("pop.gmail.com")

    def authenticate(self):
        self.gmail.user("recent:" + self.username)
        self.gmail.pass_(self.password)

    def get_number_of_messages(self):
        return self.gmail.stat()[0]

    def find_and_download_emeals_msg(self):
        message_match = 0  
        for message_counter in range(self.get_number_of_messages(), 1, -1):
            message_header = email.message_from_string("\n".join(self.gmail.top(message_counter, 0)[1]))
            from_address = message_header.get("From")
            if re.search("no-reply@emeals\.com", from_address):
                print(message_header.get("From"))
                message_match = message_counter
                break

        if message_match == 0:
            print("Unable to find an E-meals message.")
        else:
            emeals_message = email.message_from_string("\n".join(self.gmail.retr(message_match)[1]))
            for part in emeals_message.walk():
                part_filename = self.download_folder + "/" + str(part.get_filename()) 
                if re.search("\.pdf", part_filename):
                    fp = open(part_filename, "wb")
                    fp.write(part.get_payload(decode=True))
                    fp.close()
                    self.download_file_name = part_filename

    def get_download_file_path(self):
        return os.path.realpath(self.download_file_name)

    def close(self):
        self.gmail.quit()   



def main():
    
    gmailHelper = GmailHelper(username='username@gmail.com', password='')

    print("Logging into Gmail's POP3 service")
    gmailHelper.authenticate()    

    print("Finding and downloading most recent Emeals msg")
    gmailHelper.find_and_download_emeals_msg()

    print("Closing the connection")
    gmailHelper.close()
    
    print("Printing our file to the Brother printer")
    
    #call(["lpr", "-P", "Brother_HL_2280DW", gmailHelper.get_download_file_path()])
        
    
if __name__ == "__main__":
    main()